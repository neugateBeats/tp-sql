<?php
include 'connexion.php';
// Nombre d'abonnés par page
$livres_par_page = 20;

// Calcul du numéro de la page à afficher
if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

// Calcul de l'offset
$offset = ($page - 1) * $livres_par_page;

// Requête SQL pour filtrer les résultats en fonction des champs de recherche remplis
$whereClause = '';
if (!empty($_GET['titre']) || !empty($_GET['auteur']) || !empty($_GET['editeur']) || !empty($_GET['date_emprunt'])) {
    $filtres = array();
    if (!empty($_GET['titre'])) {
        $filtres[] = "livre.titre LIKE '%" . htmlspecialchars($_GET['titre']) . "%'";
    }
    if (!empty($_GET['auteur'])) {
        $filtres[] = "auteur.nom LIKE '%" . htmlspecialchars($_GET['auteur']) . "%'";
    }
    if (!empty($_GET['editeur'])) {
        $filtres[] = "editeur.nom LIKE '%" . htmlspecialchars($_GET['editeur']) . "%'";
    }
    if (!empty($_GET['date_emprunt'])) {
        $filtres[] = "emprunt.date_emprunt = '%" . htmlspecialchars($_GET['date_emprunt']) . "%'";
    }
    if (!empty($filtres)) {
        $whereClause = ' WHERE ' . implode(' AND ', $filtres);
    }
}
$livreall = "SELECT livre.titre, auteur.nom AS auteur_nom, editeur.nom AS editeur_nom, MAX(emprunt.date_emprunt) AS date_dernier_emprunt
    FROM livre 
    INNER JOIN auteur ON livre.id_auteur = auteur.id 
    INNER JOIN editeur ON livre.id_editeur = editeur.id
    INNER JOIN emprunt ON livre.id = emprunt.id_livre"
    . $whereClause . "
    GROUP BY livre.id";



?>
<!DOCTYPE html>

<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Projet</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>

    <form method="GET" class="mt-6 w-80">
        <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
        <div class="relative">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">

                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>

            </div>
            <input type="search" id="default-search" name="titre" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="titre" autocomplete="off" value="<?= (isset($_GET['titre']) ? $_GET['titre'] : '') ?>">
            <input type="search" id="default-search" name="auteur" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Auteur" autocomplete="off" value="<?= (isset($_GET['auteur']) ? $_GET['auteur'] : '') ?>">
            <input type="search" id="default-search" name="editeur" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Editeur" autocomplete="off" value="<?= (isset($_GET['editeur']) ? $_GET['editeur'] : '') ?>">
            <input type="search" id="default-search" name="date_dernier_emprunt" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Dernier emprunt" autocomplete="off" value="<?= (isset($_GET['date_dernier_emprunt']) ? $_GET['date_dernier_emprunt'] : '') ?>">
            <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
        </div>
    </form>
    <?php
    $resultat = mysqli_query($conn, $livreall);
    // Affichage des liens de pagination
    $livres_total = mysqli_num_rows($resultat);
    $pages_total = ceil($livres_total / $livres_par_page);

    for ($i = 1; $i <= $pages_total; $i++) {
        echo "<a href='?page=$i' class='inline-block px-4 py-2 bg-gray-200 rounded-lg hover:bg-gray-500 mb-2'>$i</a> ";
    }
    ?>

    <section class="affichage_livre">
        <?php
        $livreall .= " LIMIT $livres_par_page OFFSET $offset";
        $resultat = mysqli_query($conn, $livreall);
        // Vérifier si des résultats ont été trouvés
        if (mysqli_num_rows($resultat) > 0) {
            // Commencer le tableau HTML
        ?>
            <table class="table-auto">
                <thead>
                    <tr class="bg-gray-400">
                        <th class="w-1/4 px-4 py-2">Titre</th>
                        <th class="w-1/4 px-4 py-2">Nom de l'auteur</th>
                        <th class="w-1/4 px-4 py-2">Nom de l'éditeur</th>
                        <th class="w-1/4 px-4 py-2">Date du dernier emprunt</th>
                        <th class="w-1/4 px-4 py-2">Disponible ou non disponible</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($livre = mysqli_fetch_assoc($resultat)) { ?>
                        <tr class="bg-blue border-b border-gray-600">
                            <td class="w-1/4 px-4 py-2"><?= $livre['titre']; ?></td>
                            <td class="w-1/4 px-4 py-2"><?= $livre['auteur_nom']; ?></td>
                            <td class="w-1/4 px-4 py-2"><?= $livre['editeur_nom']; ?></td>
                            <td class="w-1/4 px-4 py-2"><?= $livre['date_dernier_emprunt']; ?></td>
                            <td class="w-1/4 px-4 py-2"><?= $livre['date_fin_abo']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        <?php
        } else {
            echo 'Aucun résultat trouvé.';
        }

        ?>
    </section>

</body>

</html>