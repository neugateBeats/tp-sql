<?php
include 'connexion.php';
// Nombre d'abonnés par page
$abonnes_par_page = 20;

// Calcul du numéro de la page à afficher
if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

// Calcul de l'offset
$offset = ($page - 1) * $abonnes_par_page;

// Requête SQL pour filtrer les résultats en fonction des champs de recherche remplis
$whereClause = '';
if (!empty($_GET['nom']) || !empty($_GET['prenom']) || !empty($_GET['ville']) || !empty($_GET['date_naissance']) || !empty($_GET['date_emprunt'])) {
    $filtres = array();
    if (!empty($_GET['nom'])) {
        $filtres[] = "abonne.nom LIKE '%" . htmlspecialchars($_GET['nom']) . "%'";
    }
    if (!empty($_GET['prenom'])) {
        $filtres[] = "abonne.prenom LIKE '%" . htmlspecialchars($_GET['prenom']) . "%'";
    }
    if (!empty($_GET['ville'])) {
        $filtres[] = "abonne.ville LIKE '%" . htmlspecialchars($_GET['ville']) . "%'";
    }
    if (!empty($_GET['date_naissance'])) {
        $filtres[] = "abonne.date_naissance = '%" . htmlspecialchars($_GET['date_naissance']) . "%'";
    }
    if (!empty($_GET['date_fin_abo'])) {
        $filtres[] = "abonne.date_fin_abo = '%" . htmlspecialchars($_GET['date_fin_abo']) . "%'";
    }
    if (!empty($filtres)) {
        $whereClause = ' WHERE ' . implode(' AND ', $filtres);
    }
}

$aball = "SELECT id, nom, prenom, ville, date_naissance, date_fin_abo,
            CASE 
             WHEN date_fin_abo >= CURDATE() THEN 'abonné'
             ELSE 'expiré'
            END AS etat_abonnement
            FROM abonne"
    . $whereClause . "
            ORDER BY id DESC";

?>
<!DOCTYPE html>

<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Projet</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>

    <form method="GET" class="mt-6 w-80 mb-2">
        <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
        <div class="relative">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">

                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>

            </div>
            <input type="search" id="default-search" name="nom" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nom" autocomplete="off" value="<?= (isset($_GET['nom']) ? $_GET['nom'] : '') ?>">
            <input type="search" id="default-search" name="prenom" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Prenom" autocomplete="off">
            <input type="search" id="default-search" name="ville" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Ville" autocomplete="off" value="<?= (isset($_GET['ville']) ? $_GET['ville'] : '') ?>">
            <input type="search" id="default-search" name="date_naissance" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Date de naissance" autocomplete="off" value="<?= (isset($_GET['date_naissance']) ? $_GET['date_naissance'] : '') ?>">
            <input type="search" id="default-search" name="date_fin_abo" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Date de fin" autocomplete="off" value="<?= (isset($_GET['date_fin_abo']) ? $_GET['date_fin_abo'] : '') ?>">
            <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
        </div>
    </form>

    <?php
    $resultat = mysqli_query($conn, $aball);
    // Affichage des liens de pagination
    $abonnes_total = mysqli_num_rows($resultat);
    $pages_total = ceil($abonnes_total / $abonnes_par_page);

    for ($i = 1; $i <= $pages_total; $i++) {
        echo "<a href='?page=$i' class='inline-block px-4 py-2 bg-gray-200 rounded-lg hover:bg-gray-500 mb-2'>$i</a> ";
    }
    ?>


    <section class="affichage_abonne">
        <?php
        $aball .= " LIMIT $abonnes_par_page OFFSET $offset";
        $resultat = mysqli_query($conn, $aball);

        // Vérifier si des résultats ont été trouvés
        if (mysqli_num_rows($resultat) > 0) {
            // Commencer le tableau HTML
        ?>
            <table class="table-auto">
                <thead>
                    <tr class="bg-gray-400">
                        <th class="px-4 py-2">Nom</th>
                        <th class="px-4 py-2">Prénom</th>
                        <th class="px-4 py-2">Ville</th>
                        <th class="px-4 py-2">Date de naissance</th>
                        <th class="px-4 py-2">Date de fin d'abonnement</th>
                        <th class="px-4 py-2">Etat abonnement</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($abonne = mysqli_fetch_assoc($resultat)) { ?>
                        <tr class="bg-blue border-b border-gray-600">
                            <td class="border px-4 py-2"><a href="ficheabonne.php?id=<?= $abonne['id']; ?>"><?= $abonne['nom']; ?></a></td>
                            <td class="border px-4 py-2"><?= $abonne['prenom']; ?></td>
                            <td class="border px-4 py-2"><?= $abonne['ville']; ?></td>
                            <td class="border px-4 py-2"><?= $abonne['date_naissance']; ?></td>
                            <td class="border px-4 py-2"><?= $abonne['date_fin_abo']; ?></td>
                            <td class="border px-4 py-2"><?= $abonne['etat_abonnement']; ?></td>

                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        <?php
        } else {
            echo 'Aucun résultat trouvé.';
        }

        ?>

    </section>


</body>

</html>