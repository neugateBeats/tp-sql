<?php
// Inclusion du fichier de connexion à la base de données
include 'connexion.php';

// Vérification si l'ID de l'abonné est défini dans l'URL
if (isset($_GET['id'])) {
    // Récupération de l'ID de l'abonné
    $id_abonne = $_GET['id'];

    if (isset($_POST['save'])) {
        $nouveau_nom = $_POST['nom'];
        $nouveau_prenom = $_POST['prenom'];
        $nouvelle_ville = $_POST['ville'];
        $nouvelle_date_naissance = $_POST['date_naissance'];
        $nouvelle_date_inscription = $_POST['date_inscription'];
        $nouvelle_date_fin_abo = $_POST['date_fin_abo'];

        $sql = "UPDATE abonne SET nom = '$nouveau_nom', prenom = '$nouveau_prenom', ville = '$nouvelle_ville', date_naissance = '$nouvelle_date_naissance', date_fin_abo = '$nouvelle_date_fin_abo', date_inscription ='$nouvelle_date_inscription' WHERE id = '$id_abonne'";
        $requete = $conn->prepare($sql);
        $requete->execute();
    }

    // Requête SQL pour récupérer les informations de l'abonné correspondant à l'ID
    $requete = $conn->prepare('SELECT nom, prenom,ville, date_naissance, date_inscription, date_fin_abo  FROM abonne WHERE id = ?');
    $requete->bind_param('i', $id_abonne);
    $requete->execute();
    $requete->bind_result($nom, $prenom, $ville, $date_naissance, $date_inscription, $date_fin_abo);
    $abonne = $requete->fetch();

    // Requête SQL pour récupérer la liste des livres empruntés par l'abonné correspondant à l'ID
    // $livre = $conn->prepare('SELECT livre.titre, livre.genre, livre.categorie
    //  FROM emprunt
    //  INNER JOIN livre ON emprunt.id_livre = livre.id
    //  ORDER BY emprunt.date_emprunt DESC');
    // $livre->bind_param('i', $id_abonne);
    // $livre->execute();
    // $abonne = $livre->fetch();

    // Affichage des informations de l'abonné
    if ($abonne) {
?>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset='utf-8'>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <title>Projet</title>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
            <script src='main.js'></script>
            <script src="https://cdn.tailwindcss.com"></script>
        </head>

        <body>

            <div class="bg-white  rounded-lg shadow-md overflow-hidden">
                <h1 class="text-center text-lg font-bold">LA FICHE D'UN ABONNE</h1>
                <div class="px-6 py-4">
                    <h1 class="text-lg font-bold mb-2">Informations de l'abonné</h1>
                    <p class="text-gray-700"><span class="font-bold">Nom :</span> <?php echo $nom; ?></p>
                    <p class="text-gray-700"><span class="font-bold">Prénom :</span> <?php echo $prenom; ?></p>
                    <p class="text-gray-700"><span class="font-bold">Ville :</span> <?php echo $ville; ?></p>
                    <p class="text-gray-700"><span class="font-bold">Date de naissance :</span> <?php echo $date_naissance; ?></p>
                    <p class="text-gray-700"><span class="font-bold">Date de fin d'abonné :</span> <?php echo $date_fin_abo; ?></p>
                    <p class="text-gray-700"><span class="font-bold">Date inscription :</span> <?php echo $date_inscription; ?></p>
                </div>
            </div>

            <div class="bg-white rounded-lg shadow-md overflow-hidden mt-6">
                <h1 class="text-center text-lg font-bold">Modifier les informations de l'abonné</h1>
                <div class="px-6 py-4">
                    <form action="ficheabonne.php?id=<?php echo $id_abonne; ?>" method="POST">
                        <div class="mb-4">
                            <label class="block text-gray-700 font-bold mb-2" for="nom">
                                Nom :
                            </label>
                            <input class="border rounded-md px-3 py-2 w-full" type="text" name="nom" id="nom" value="<?php echo $nom; ?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-700 font-bold mb-2" for="prenom">
                                Prénom :
                            </label>
                            <input class="border rounded-md px-3 py-2 w-full" type="text" name="prenom" id="prenom" value="<?php echo $prenom; ?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-700 font-bold mb-2" for="ville">
                                Ville :
                            </label>
                            <input class="border rounded-md px-3 py-2 w-full" type="text" name="ville" id="ville" value="<?php echo $ville; ?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-700 font-bold mb-2" for="date_naissance">
                                Date de naissance :
                            </label>
                            <input class="border rounded-md px-3 py-2 w-full" type="date" name="date_naissance" id="date_naissance" value="<?php echo $date_naissance; ?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-700 font-bold mb-2" for="date_inscription">
                                Date d'inscription :
                            </label>
                            <input class="border rounded-md px-3 py-2 w-full" type="date" name="date_inscription" id="date_inscription" value="<?php echo $date_inscription; ?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-700 font-bold mb-2" for="date_fin_abo">
                                Date de fin d'abonnement :
                            </label>
                            <input class="border rounded-md px-3 py-2 w-full" type="date" name="date_fin_abo" id="date_fin_abo" value="<?php echo $date_fin_abo; ?>">
                        </div>
                        <div class="flex justify-end">
                            <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600" name="save">Modifier</button>
                        </div>
                    </form>
                </div>
            </div>



        </body>

        </html>
<?php
    } else {
        echo "L'abonné n'existe pas.";
    }
} else {
    echo "L'ID de l'abonné n'est pas défini.";
}
?>